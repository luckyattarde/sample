<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public $controller_data;
	public function __construct(){
		session_start();
		$this->controller_data = array();
		parent::__construct();
		
	}


	public function index()
	{
		$this->mainview();
	
	}


	public function mainview() {		// mainview controller method
		

		$this->form_validation->set_rules('email', 'Email', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('mainview');
		}
		else
		{
			$this->load->view('mainview');
		}

	}

	public function addpost() {

		$this->form_validation->set_rules('title', 'Title', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('addpost', $this->controller_data);
		}
		else
		{
			$data = $this->input->post();

			$postid = $this->main_model->addpost($data);
			redirect('main/editpost/'.$postid);
		}


	}




	public function addcategory() {

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('addcategories', $this->controller_data);
		}
		else
		{
			$data = $this->input->post();

			$categoryid = $this->main_model->addcategory($data);
			redirect('main/editcategory/'.$categoryid);
		}


	}




	public function addauthor() {

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('addauthor', $this->controller_data);
		}
		else
		{
			$data = $this->input->post();

			$authorid = $this->main_model->addauthor($data);
			redirect('main/editauthor/'.$authorid);
		}


	}



	public function editauthor($authorid) {
	

		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{	$this->controller_data['data'] = $this->main_model->get_author_perid($authorid);
			$this->load->view('editauthor', $this->controller_data);
		}
		else
		{
			$data = $this->input->post();
			$this->session->set_userdata('authorid', $authorid);
		
			$data['authorid'] = $authorid;
			$this->main_model->updateauthor($data);	// execute update SQL

			if($data['status'] == 2 ) {		// can do it more nice using redirect to improve interface

				$this->main_model->remove(array('columnname'=>'authorid', 'columnid'=>$authorid),'authors');
				echo "Deleted";
				exit();
			}

				// if image is set

	if((isset($_FILES['image_location'])) && ($_FILES['image_location']['tmp_name'] != null)) {

					if (!file_exists('./uploads/')) {	// create folder if not exists
					mkdir('./uploads/');
					}	
						// image upload code 
					$config['upload_path'] = './uploads/';
					$config['allowed_types'] = 'gif|jpg|png';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);
						if ( !$this->upload->do_upload('image_location'))
						{
							$error = array('error' => $this->upload->display_errors());
							print_r($error);
							
						}else
						{
							$upload_data = $this->upload->data();
							
						}

						if(isset($upload_data['full_path'])) {		// add path to DB
							$data['image_location'] = $upload_data['full_path'];
						}

					$this->main_model->updateauthor($data);	// execute update SQL
					$this->request_dropbox();	// calling dropbox API
					
		}


			

			$this->controller_data['data'] = $this->main_model->get_author_perid($authorid);
			$this->load->view('editauthor', $this->controller_data);
		}


	}

	

	   public function request_dropbox()
	{
		$params['key'] = 'rnfnzku1rcdqjv0';    
		$params['secret'] = 'p02gyefpt3fa638';
				
		$this->load->library('dropbox', $params);
		$data = $this->dropbox->get_request_token(site_url("main/access_dropbox"));

		$this->session->set_userdata('token_secret', $data['token_secret']);
		redirect($data['redirect']);
	}
	//This method should not be called directly, it will be called after 
    //the user approves your application and dropbox redirects to it
	public function access_dropbox()
	{
		$params['key'] = 'rnfnzku1rcdqjv0';    
		$params['secret'] = 'p02gyefpt3fa638';
				
				
		$this->load->library('dropbox', $params);
	
		$oauth = $this->dropbox->get_access_token($this->session->userdata('token_secret'));

		$this->session->set_userdata('oauth_token', $oauth['oauth_token']);
		$this->session->set_userdata('oauth_token_secret', $oauth['oauth_token_secret']);
        redirect('main/test_dropbox');
	}
	
	public function test_dropbox()
	{
		

		$params['key'] = 'rnfnzku1rcdqjv0';    
		$params['secret'] = 'p02gyefpt3fa638';

		$params['access'] = array('oauth_token'=>urlencode($this->session->userdata('oauth_token')),
		'oauth_token_secret'=>urlencode($this->session->userdata('oauth_token_secret')));

		$this->load->library('dropbox', $params);

		$dbobj = $this->dropbox->account();
		
		$author_data = $this->main_model->get_author_perid($this->session->userdata('authorid'));

		//echo $author_data->image_location; die();
		($this->dropbox->add('/'.$author_data->name.'.png',$author_data->image_location));
	


		redirect('main/editauthor/'.$this->session->userdata('authorid'));
	
	}


	public function editcategory($categoryid) {
			$this->controller_data['data'] = $this->main_model->get_category_perid($categoryid);
			$this->controller_data['posts'] = $this->main_model->get_all_posts();
	
		$this->form_validation->set_rules('name', 'Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('editcategories', $this->controller_data);
		}
		else
		{
			$data = $this->input->post();
			$data['categoryid'] = $categoryid;



			$post_array = array();
			$post_array = $this->input->post('postid');
			$this->main_model->remove_posts($categoryid);

			if($post_array != null ) {
			foreach ($post_array as $key => $value) {
				$this->main_model->add_post_cat(array('postid'=>$value, 'categoryid'=>$categoryid));
			}

			}

			unset($data['postid']);


			$this->main_model->udpatecategory($data);



			if($data['status'] == 2 ) {

				$this->main_model->remove(array('columnname'=>'categoryid', 'columnid'=>$categoryid),'postscategories');
				echo "Deleted";
				exit();
			}

			$this->load->view('editcategories', $this->controller_data);
		}


	}



	public function editpost($postid) {
		$this->controller_data['data'] = $this->main_model->get_post_perid($postid);
		$this->controller_data['categories'] = $this->main_model->get_all_post_categories();
		$this->controller_data['author_data'] = $this->main_model->get_all_authors();
		$this->form_validation->set_rules('title', 'Title', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('editpost', $this->controller_data);
		}
		else
		{
			$data = $this->input->post();
			$data['postid'] = $postid;

			$cate_array = array();
			$cate_array = $this->input->post('categoryid');
			$this->main_model->remove_categories($postid);

			if($cate_array != null ) {
			foreach ($cate_array as $key => $value) {
				$this->main_model->add_post_cat(array('postid'=>$postid, 'categoryid'=>$value));
			}

			}

			unset($data['categoryid']);
			
			$this->main_model->updatepost($data);


			if($data['status'] == 2 ) {

				$this->main_model->remove(array('columnname'=>'postid', 'columnid'=>$postid),'posts');
				echo "Deleted";
				exit();
			}



			$this->load->view('editpost', $this->controller_data);
		}

	}






}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */