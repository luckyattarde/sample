<?php
/*
- library to include, in autoload
- contains all site config data ...
*/
class site_settings {

	public $sitebase, $sitehost, $skinid, $skinpath, $skinurl, $siteid,  $hosttype;
	
	public function __construct(){
		
		$this->sitebase = "c";
		$this->sitehost = "http://".$_SERVER['HTTP_HOST']."/".$this->sitebase; // the full site host, i.e. for local images
		$this->hosttype = "http://".$_SERVER['HTTP_HOST'];
		$this->skinid = 0;
		$this->skinpath = "skins/";
		$this->skinurl = $this->sitehost."/".$this->skinpath;
		$this->siteid = 2;
	}
	
}

?>