<?php
class main_model extends CI_Model
{
	
	public function __construct(){
	
	}

	

	public  function get_all_post_categories() {			// return all   post categories {no conditions here)
		$select = $this->db->get('postscategories');
		return $select->result();
	}
	public  function get_all_authors() {				// return all users {no conditions here)
		$select = $this->db->get('authors');
		return $select->result();
	}

	public function get_all_posts() {
		$select = $this->db->get('posts');
		return $select->result();

	}


	public function get_author_perid($authorid) {		
		$this->db->where('authorid',$authorid);
		$select = $this->db->get('authors');
		return $select->row();
	}

	public function get_post_perid($postid) {		
		$this->db->where('postid',$postid);
		$select = $this->db->get('posts');
		return $select->row();
	}
	public function get_category_perid($categoryid) {		
		$this->db->where('categoryid',$categoryid);
		$select = $this->db->get('postscategories');
		return $select->row();
	}



	public function addpost($data) {

		$this->db->insert('posts', $data);
		return $this->db->insert_id();
	}

	public function addcategory($data)  {

	$this->db->insert('postscategories', $data);
		return $this->db->insert_id();		
	}


	public function addauthor($data) {
		
		$this->db->insert('authors', $data);
		return $this->db->insert_id();
	}


	public function remove_categories($postid) {
		$this->db->where('postid',$postid);
		$select = $this->db->delete('post_cat_rel');
	}

	public function add_post_cat($data) {

	
		$select = $this->db->insert('post_cat_rel', $data);


	}

	public function get_posts_perauthor($authorid) {

		$this->db->where('authorid',$authorid);
		$select = $this->db->get('posts');
		return $select->result();
	} 




	public function remove_posts($categoryid) {
		$this->db->where('categoryid',$categoryid);
		$select = $this->db->delete('post_cat_rel');
	}

	




	public function updatepost($data) {
		$this->db->where('postid',$data['postid']);
		$this->db->update('posts', $data);
	
	}

	public function udpatecategory($data)  {
		$this->db->where('categoryid',$data['categoryid']);
	$this->db->update('postscategories', $data);
			
	}


	public function updateauthor($data) {
		$this->db->where('authorid',$data['authorid']);

		$this->db->update('authors', $data);
		
	}

	public function remove($data, $table) {


		$this->db->where($data['columnname'], $data['columnid']);
		$this->db->delete(''.$table.'');
		return true;
	}







}