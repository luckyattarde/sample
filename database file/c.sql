-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 30, 2014 at 05:26 AM
-- Server version: 5.6.13
-- PHP Version: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c`
--
CREATE DATABASE IF NOT EXISTS `c` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `c`;

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE IF NOT EXISTS `authors` (
  `authorid` int(4) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `image_location` text NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`authorid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`authorid`, `name`, `email`, `image_location`, `status`) VALUES
(2, 'author1', 'luckyattarde@gmail.com', '', 0),
(3, 'kj;lkj', 'luckyattarde@gmail.com', '', 0),
(4, 'kj;lkj', 'luckyattarde@gmail.com', '', 0),
(5, 'rtt', 'luckyattarde@gmail.com', '', 0),
(6, 'authaf', 'luckyattarde1@gmail.com', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `postid` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `authorid` int(4) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`postid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`postid`, `title`, `content`, `authorid`, `date`, `date_updated`, `status`) VALUES
(1, 'post 2', 'kjjlk', 1, '2014-07-15 12:00:00', '2014-05-29 23:52:27', 0),
(2, 'sdfsd', 'kjjlk', 0, '2014-05-30 00:30:51', '2014-05-30 00:05:20', 0),
(4, 'ppost', 'dklfaj', 4, '2014-06-17 12:00:00', '2014-05-30 00:57:34', 0);

-- --------------------------------------------------------

--
-- Table structure for table `postscategories`
--

CREATE TABLE IF NOT EXISTS `postscategories` (
  `categoryid` int(4) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`categoryid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `postscategories`
--

INSERT INTO `postscategories` (`categoryid`, `name`, `status`) VALUES
(2, 'cate2', 0),
(3, 'cafad', 0),
(4, 'cat44', 0);

-- --------------------------------------------------------

--
-- Table structure for table `post_cat_rel`
--

CREATE TABLE IF NOT EXISTS `post_cat_rel` (
  `categoryid` int(4) NOT NULL,
  `postid` int(4) NOT NULL,
  PRIMARY KEY (`categoryid`,`postid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_cat_rel`
--

INSERT INTO `post_cat_rel` (`categoryid`, `postid`) VALUES
(4, 1),
(4, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
