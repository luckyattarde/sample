<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>C</title>

<link rel="stylesheet" href="<?php echo $this->site_settings->skinurl ?>css/style.css" />

<link rel="stylesheet" href="<?php echo $this->site_settings->skinurl ?>css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo $this->site_settings->skinurl ?>css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="<?php echo $this->site_settings->skinurl ?>css/jquery-ui-1.10.4.custom.min.css" />
<script type="text/javascript" src="<?php echo $this->site_settings->skinurl ?>js/jquery.js"></script>
<script type="text/javascript" src="<?php echo $this->site_settings->skinurl ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->site_settings->skinurl ?>js/jquery-ui-1.10.4.custom.min.js"></script>

<script>
  $(function() {
    $( "#date_started" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: "yy-mm-dd",
      onClose: function( selectedDate ) {
        $( "#date_complete" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#date_complete" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: "yy-mm-dd" ,
      onClose: function( selectedDate ) {
        $( "#date_started" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });


  $(function() {

      $( "#date" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      dateFormat: "yy-mm-dd",
      onClose: function( selectedDate ) {
        $( "#date_complete" ).datepicker( "option", "minDate", selectedDate );
      }
    });
  });
  </script>


</head>
<body>
<div id="header">

<ul>

<li><a href="<?php echo $this->site_settings->sitehost ?>/main/addauthor">add author</a></li>


<li><a href="<?php echo $this->site_settings->sitehost ?>/main/addcategory">add category</a></li>



<li><a href="<?php echo $this->site_settings->sitehost ?>/main/addpost">add post</a></li>

</ul>
</div>
